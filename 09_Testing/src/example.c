#include <buf.h>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/** region Example usage */
int main(void)
{
    float *values = NULL;
    for (size_t i = 0; i < 25; i++)
        buf_push(values, rand() / (float)RAND_MAX);

    for (size_t i = 0; i < buf_size(values); i++)
        printf("values[%zu] = %f\n", i, values[i]);

    buf_free(values);
}
/** endregion */
