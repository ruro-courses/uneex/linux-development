#include <check.h>
#include <sys/resource.h>

#include "buf.h"

#suite Symbols
#tcase buf_grow1
#test buf_grow1_exists
	ck_assert_ptr_nonnull(buf_grow1);

#suite Functionality

#tcase Initialization
#test init_empty
    float *a = 0;
    ck_assert_msg(buf_capacity(a) == 0, "capacity init");
    ck_assert_msg(buf_size(a) == 0, "size init");
    buf_free(a);
    ck_assert_msg(a == 0, "free no-op");

#test init_push
    float *a = 0;
    buf_push(a, 1.3f);
    ck_assert_msg(buf_size(a) == 1, "size 1");
    ck_assert_msg(a[0] == (float)1.3f, "value");
    buf_free(a);
    ck_assert_msg(a == 0, "free");

#test init_clear_empty
    float *a = 0;
    buf_clear(a);
    ck_assert_msg(buf_size(a) == 0, "clear empty");
    ck_assert_msg(a == 0, "clear no-op");
    buf_free(a);
    ck_assert_msg(a == 0, "free no-op");

#test init_clear
    float *a = 0;
    buf_push(a, 1.3f);
    buf_clear(a);
    ck_assert_msg(buf_size(a) == 0, "clear");
    ck_assert_msg(a != 0, "clear not-free");
    buf_free(a);
    ck_assert_msg(a == 0, "free");

#tcase Indexing
#test indexing_push
    long *a = 0;
    for (int i = 0; i < 10000; i++)
        buf_push(a, i);
    ck_assert_msg(buf_size(a) == 10000, "size 10000");
    buf_free(a);
    ck_assert_msg(a == 0, "free");

#test indexing_check
    long *a = 0;
    for (int i = 0; i < 10000; i++)
        buf_push(a, i);
    for (int i = 0; i < (int)(buf_size(a)); i++)
        ck_assert_msg(a[i] == i, "element #%d", i);
    buf_free(a);
    ck_assert_msg(a == 0, "free");

#tcase Resizing
#test resizing_grow
    long *a = 0;
    buf_grow(a, 1000);
    ck_assert_msg(buf_capacity(a) == 1000, "grow 1000");
    ck_assert_msg(buf_size(a) == 0, "size 0 (grow)");
    buf_free(a);
    ck_assert_msg(a == 0, "free");

#test resizing_trunc_empty
    long *a = 0;
    buf_grow(a, 1000);
    buf_trunc(a, 100);
    ck_assert_msg(buf_capacity(a) == 100, "trunc 100");
    ck_assert_msg(buf_size(a) == 0, "size 0 (trunc)");
    buf_free(a);
    ck_assert_msg(a == 0, "free");

#test resizing_trunc
    long *a = 0;
    for (int i = 0; i < 1000; i++)
        buf_push(a, i);
    ck_assert_msg(buf_size(a) == 1000, "size 1000");
    buf_trunc(a, 100);
    ck_assert_msg(buf_capacity(a) == 100, "trunc 100");
    ck_assert_msg(buf_size(a) == 100, "size 100 (trunc)");
    buf_free(a);
    ck_assert_msg(a == 0, "free");

#tcase Stack
#test stack
    float *a = 0;
    buf_push(a, 1.1);
    buf_push(a, 1.2);
    buf_push(a, 1.3);
    buf_push(a, 1.4);
    ck_assert_msg(buf_size(a) == 4, "size 4");
    ck_assert_msg(buf_pop(a) == (float)1.4f, "pop 3");
    buf_trunc(a, 3);
    ck_assert_msg(buf_size(a) == 3, "size 3");
    ck_assert_msg(buf_pop(a) == (float)1.3f, "pop 2");
    ck_assert_msg(buf_pop(a) == (float)1.2f, "pop 1");
    ck_assert_msg(buf_pop(a) == (float)1.1f, "pop 0");
    ck_assert_msg(buf_size(a) == 0, "size 0 (pop)");
    buf_free(a);

#suite Errors

typedef char big_t[1024 * 1024];

#tcase Memory
#test-exit(42) memory_overflow_empty
    big_t *a = 0;
    buf_grow(a, PTRDIFF_MAX); // This should trigger an abort()

#test-exit(42) memory_overflow
    big_t *a = 0;
    buf_grow(a, 1);
    buf_grow(a, PTRDIFF_MAX); // This should trigger an abort()

#test-exit(42) memory_oom_empty
    big_t *a = 0;

    size_t max_size = 32 * sizeof(*a);
    struct rlimit restricted;
    restricted.rlim_cur = max_size;
    restricted.rlim_max = max_size;
    ck_assert_msg(setrlimit(RLIMIT_AS, &restricted) != -1, "prlimit");

    buf_grow(a, 128); // This should trigger an abort()

#test-exit(42) memory_oom
    big_t *a = 0;
    buf_grow(a, 1);

    size_t max_size = 32 * sizeof(*a);
    struct rlimit restricted;
    restricted.rlim_cur = max_size;
    restricted.rlim_max = max_size;
    ck_assert_msg(setrlimit(RLIMIT_AS, &restricted) != -1, "prlimit");

    buf_grow(a, 128); // This should trigger an abort()
