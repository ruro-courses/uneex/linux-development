#include "common.h"

/** region IO, Help and Usage */
#define wrn(name, format, ...)                                                  \
do {                                                                            \
    fprintf(stderr, "%s: " format "\n", name, ## __VA_ARGS__);                  \
} while (false)

int usage(const char *prog)
{
    wrn(
        "Usage",
        "\n"
        "    %s [regexp] [substitution] [string]\n"
        "\n"
        "    Loosely equivalent to\n"
        "\n"
        "        printf '%%s\\n' [string] | sed -E 's/[regexp]/[substitution]/'\n"
        "\n"
        "Examples:\n"
        "    %s 'b' 'x' 'aabbccbb' # 'aaxbccbb'\n"
        "    %s 'bb.*bb' 'xxyy' 'aabbbbccbb' # 'aaxxyy'\n"
        "    %s 'bb(.*)bb' 'xx\\1yy' 'aabbbbccbb' # 'aaxxbbccyy'\n"
        "    %s 'bb(.*)bb' 'xx\\\\1yy' 'aabbbbccbb' # 'aaxx\\1yy'\n"
        "    %s 'missing' 'something' 'lorem ipsum' # 'lorem ipsum'\n",
        prog, prog, prog, prog, prog, prog
    );
    return EX_USAGE;
}

void puts_substring(char *str, int start, int end)
{
    // Temporarily crop the string at end
    char tmp = str[end];
    if (end != -1)
        str[end] = '\0';

    // Print it
    fputs(str + start, stdout);

    // Restore string crop
    str[end] = tmp;
}
/** endregion */

/** region Main **/
int main(int argc, char *argv[])
{
    // Check args, print usage
    if (argc != 4)
        return usage(argv[0]);

    // Compile the regular expression
    regex_t preg;
    int errcode = regcomp(&preg, argv[1], REG_NEWLINE | REG_EXTENDED);
    if (errcode)                                                                
    {                                                                           
        int errbuf_size = regerror(errcode, &preg, NULL, 0);                    
        char *errbuf = malloc(errbuf_size);                                     
        regerror(errcode, &preg, errbuf, errbuf_size);                          
        wrn("Couldn't compile the regular expression", "\n\n%s", errbuf);       
        free(errbuf);                                                           
        exit(EX_DATAERR);                                                       
    }                                                                           

    // Try to find the first match
    const size_t num_groups = '9' - '0' + 1;
    regmatch_t pmatch[num_groups];
    if (!regexec(&preg, argv[3], num_groups, pmatch, 0))
    {
        // Print the part of the string before the match
        puts_substring(argv[3], 0, pmatch->rm_so);

        // Print the substitution string
        int subs_so = 0;
        char *backslash;
        while ((backslash = strchr(argv[2] + subs_so, '\\')))
        {
            // Print string until backslash
            int next_so = backslash - argv[2];
            puts_substring(argv[2], subs_so, next_so);
            next_so++;

            // Parse backslash
            int index = argv[2][next_so];
            if (index == '\\')
            {
                putc('\\', stdout);
                subs_so = next_so + 1;
                continue;
            }
            if ((index < '0') || (index > '9'))
            {
                wrn(
                    "Escape sequences other than group substitutions (\\0-\\9) "
                    "and backslash escape (\\\\) are not supported",
                    "\\%c", argv[2][next_so]
                );
                __fpurge(stdout);
                exit(EX_DATAERR);
            }

            // Get referenced group
            index -= '0';
            if (pmatch[index].rm_so == -1)
            {
                wrn(
                    "Invalid reference",
                    "\\%c", argv[2][next_so]
                );
                __fpurge(stdout);
                exit(EX_DATAERR);
            }
            next_so++;

            // Print the referenced group
            puts_substring(argv[3], pmatch[index].rm_so, pmatch[index].rm_eo);

            // Advance iteration
            subs_so = next_so;
        }
        // Print the remaining part of the substitution string
        puts_substring(argv[2], subs_so, -1);

        // Print the remaining part of the string
        puts_substring(argv[3], pmatch->rm_eo, -1);
    }
    else
    {
        // No match was found, just print the original string
        fputs(argv[3], stdout);
    }

    // Print the final newline, clean up and exit
    putc('\n', stdout);
    fflush(stdout);
    regfree(&preg);
    return 0;
}
/** endregion */
