set -ex

function esub_vs_sed() {
    cmp \
        <(./esub "${@}") \
        <(printf "%s\n" "${3}" | sed -E "s/${1}/${2}/")
}
