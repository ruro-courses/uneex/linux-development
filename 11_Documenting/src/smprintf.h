#pragma once
#include "common.h"

/** @addtogroup smprintf */
/** @{ */
/** @file smprintf.h */

/** va_list version of @ref smprintf. */
char *vsmprintf(const char *format, va_list args1);

/** Format-print to malloc allocated string.
 *
 * Behaves exactly like snprintf, except that an output buffer of sufficient
 * size is allocated automatically.
 *
 * @warning The returned string is allocated on the heap with malloc.
 * It is the responsibility of the user to free this memory after use.
 */
char *smprintf(const char *format, ...);

/** @} */
