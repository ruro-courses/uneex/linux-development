#pragma once
#include "common.h"

/** @addtogroup roman */
/** @{ */
/** @file roman.h */

/** Specification for a single type of roman numeral.
 *
 * Here we treat "subtraction" as a separate kind of numeral instead of as a
 * property of the number system. For example the number CIX (109) is treated
 * as the sequence C, IX (100 + 9) instead of as C, I, X (100 - 1 + 10). This
 * significantly simplifies the parsing logic.
 */
typedef struct
{
    char name[2]; ///< 1 or 2 characters for the roman numeral.
    int value; ///< The numerical value of the numeral.
} numeral_t;

/** Convert the number decimal to a roman numeral string.
 *
 * Supports roman numerals from 0 (N) up to 4999 (MMMMCMXCIX).
 *
 * @warning The returned string is allocated on the heap with malloc.
 * It is the responsibility of the user to free this memory after use.
 */
char *decimal_to_roman(int decimal);

/** Parse the roman numeral string roman and return its numerical value.
 *
 * Supports roman numerals from 0 (N) up to 4999 (MMMMCMXCIX).
 *
 * If roman is not a valid roman numeral, return -1 instead.
 */
int roman_to_decimal(const char *roman);

/** @} */
