#include "smprintf.h"

/** @addtogroup smprintf */
/** @{ */
/** @file smprintf.c */

char *vsmprintf(const char *format, va_list args1)
{
    va_list args2;
    va_copy(args2, args1);

    size_t size = 1 + vsnprintf(NULL, 0, format, args1);

    char *buffer = malloc(size);
    vsnprintf(buffer, size, format, args2);

    return buffer;
}

char *smprintf(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    char *buffer = vsmprintf(format, args);
    va_end(args);
    return buffer;
}

/** @} */
