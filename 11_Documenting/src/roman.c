#include "roman.h"

/** @addtogroup roman */
/** @{ */
/** @file roman.c */

/** Convenient alias for the null byte. */
#define ___ '\0' // NOLINT(bugprone-reserved-identifier)

/** All ASCII-only roman numerals. */
const numeral_t numeral_map[] =
{
    {.name = {'M', ___}, .value = 1000},
    {.name = {'C', 'M'}, .value = 900},
    {.name = {'D', ___}, .value = 500},
    {.name = {'C', 'D'}, .value = 400},
    {.name = {'C', ___}, .value = 100},
    {.name = {'X', 'C'}, .value = 90},
    {.name = {'L', ___}, .value = 50},
    {.name = {'X', 'L'}, .value = 40},
    {.name = {'X', ___}, .value = 10},
    {.name = {'I', 'X'}, .value = 9},
    {.name = {'V', ___}, .value = 5},
    {.name = {'I', 'V'}, .value = 4},
    {.name = {'I', ___}, .value = 1},
    {.name = {___, ___}, .value = 0},
};
#undef ___

/** Get the length of a @ref numeral_t (either 1 or 2 characters). */
#define numeral_length(numeral) ((numeral)->name[1] == '\0' ? 1 : 2)

/** Copy the text representation of a single numeral to the roman string. */
int put_numeral(char *roman, const numeral_t *numeral)
{
    int length = numeral_length(numeral);

    for (int i = 0; i < length; ++i)
        roman[i] = numeral->name[i];

    return length;
}

/** Check if the next 1 or 2 chars in the roman string match the numeral. */
bool check_numeral(const char *roman, const numeral_t *numeral)
{
    int length = numeral_length(numeral);

    for (int i = 0; i < length; ++i)
        if (roman[i] != numeral->name[i])
            return false;

    return true;
}

/** Apply run the body macro for each numeral in the @ref numeral_map. */
#define dec_to_rom(body)                                                        \
    size_t offset = 0;                                                          \
    const numeral_t *numeral = numeral_map;                                     \
    do                                                                          \
    {                                                                           \
        { body }                                                                \
        ++numeral;                                                              \
    }                                                                           \
    while (numeral->name[0] != '\0')

/** Body of @ref get_roman_length */
#define get_roman_length_body                                                   \
        int repetitions = decimal / numeral->value;                             \
        decimal -= repetitions * numeral->value;                                \
        offset += repetitions * numeral_length(numeral);

/** Calculate the required buffer size to store a roman numeral. */
size_t get_roman_length(int decimal)
{
    if (!decimal)
        return 1;

    dec_to_rom(get_roman_length_body);

    return offset;
}

/** Body of @ref decimal_to_roman */
#define decimal_to_roman_body                                                   \
        int repetitions = decimal / numeral->value;                             \
        decimal -= repetitions * numeral->value;                                \
        for (int i = 0; i < repetitions; ++i)                                   \
            offset += put_numeral(&roman[offset], numeral);

char *decimal_to_roman(int decimal)
{
    size_t length = get_roman_length(decimal);
    char *roman = malloc(1 + length);
    roman[0] = 'N';
    roman[length] = '\0';

    if (!decimal)
        return roman;

    dec_to_rom(decimal_to_roman_body);

    return roman;
}

/** Body of @ref roman_to_decimal */
#define roman_to_decimal_body                                                   \
        while (check_numeral(&roman[offset], numeral))                          \
        {                                                                       \
            offset += numeral_length(numeral);                                  \
            value += numeral->value;                                            \
        }

int roman_to_decimal(const char *roman)
{
    int value = 0;
    if (roman[0] == 'N' && roman[1] == '\0')
        return 0;

    dec_to_rom(roman_to_decimal_body);

    if (roman[offset])
        value = -1;
    return value;
}

/** @} */
