#pragma once

#include <locale.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include <argp.h>
#include <libintl.h>
#include <readline/history.h>
#include <readline/readline.h>

#include "config.h"

/** @addtogroup common */
/** @{ */
/** @file common.h */

/** Translate text to the current locale using gettext. */
#define _(x) gettext(x) // NOLINT(bugprone-reserved-identifier)

/** Mark text for extraction with xgettext but don't translate it at runtime. */
#define N_(x) x

/** Mark text for extraction with xgettext and remove it at compile time. */
#define S_(x)

/** @} */
