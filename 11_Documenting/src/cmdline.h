#pragma once
#include "common.h"

#include "numbers.h"

/** @addtogroup cmdline */
/** @{ */
/** @file cmdline.h */

/** Global program settings storage type. */
typedef struct
{
    number_t mode; ///< Current representation mode for number strings.
    int lo; ///< Lower bound for the guessing game
    int hi; ///< Upper bound for the guessing game
} settings_t;

/** Current program settings as parsed by @ref parse_args. */
extern settings_t settings;

/** Parse command line arguments and initialize @ref settings.
 *
 * Print usage information and exit if an error occurred.
 */
void parse_args(int argc, char *argv[]);

/** @} */
