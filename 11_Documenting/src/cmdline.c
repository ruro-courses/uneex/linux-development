#include "cmdline.h"

/** @addtogroup cmdline */
/** @{ */
/** @file cmdline.c */
/** @} */

/** Print an argp error and return EINVAL. */
#define invalid(format, ...)                                                    \
    argp_error(state, format, ##__VA_ARGS__), EINVAL

/** Convert and validate the string arg to number, store the result in ref. */
error_t parse_num(int *ref, char *arg, struct argp_state *state)
{
    int num = *ref = str_to_num(arg);

    if (num != -1)
        return 0;

    return invalid(
        _(
            "'%s' is not a valid decimal, octal, "
            "hexadecimal number or roman numeral."
        ),
        arg
    );
}

/** Convert mode specified by key to @ref number_t and store it. */
error_t parse_mode(char key, struct argp_state *state)
{
    int mode = -1;
    switch (key)
    {
    case 'd':
        mode = settings.mode = MODE_DEC;
        break;
    case 'o':
        mode = settings.mode = MODE_OCT;
        break;
    case 'x':
        mode = settings.mode = MODE_HEX;
        break;
    case 'r':
        mode = settings.mode = MODE_ROM;
        break;
    default:
        break;
    }

    if (mode != -1)
        return 0;

    return invalid(
        _("'%c' is not a valid mode."),
        key
    );
}

/** Process argp options. */
error_t parser(int key, char *arg, struct argp_state *state)
{
    (void) arg;
    (void) state;
    switch (key)
    {
        // Modes
    case 'd':
    case 'o':
    case 'x':
    case 'r':
        return parse_mode((char) key, state);
    case 'm':
        if (arg[0] && !arg[1])
            return parse_mode(arg[0], state);
        else
            return invalid(
                _(
                    "'%s' is not a valid mode. "
                    "(mode must be a single character)"
                ),
                arg
            );

        // Bounds
    case 'l':
        return parse_num(&settings.lo, arg, state);
    case 'h':
        return parse_num(&settings.hi, arg, state);

        // Sanity check
    case ARGP_KEY_END:
        if (settings.lo < 0)
            return invalid(
                _("lo must be a non-negative integer.")
            );

        if (settings.hi <= settings.lo)
            return invalid(
                _("hi must be greater than lo.")
            );

        if (settings.mode == MODE_ROM && settings.hi >= 5000)
            return invalid(
                _("hi must be less than 5000 in roman numeral mode.")
            );

        return 0;

        // Error
    default:
        return ARGP_ERR_UNKNOWN;
    }
}

/** Argp options specification. */
const struct argp_option options[] =
{
    // Modes
    {
        .key='m', .group=0, .name="mode", .arg="[doxr]",
        .doc=N_("Select which numbering system to use"),
    },

    {.key='d', .group=1, .name="dec", .doc=N_("Use decimal numbers")},
    {.key='d', .group=1, .name="decimal", .flags=OPTION_ALIAS},

    {.key='o', .group=2, .name="oct", .doc=N_("Use octal numbers")},
    {.key='o', .group=2, .name="octal", .flags=OPTION_ALIAS},

    {.key='x', .group=3, .name="hex", .doc=N_("Use hexadecimal numbers")},
    {.key='x', .group=3, .name="hexadecimal", .flags=OPTION_ALIAS},

    {.key='r', .group=4, .name="rom", .doc=N_("Use roman numerals")},
    {.key='r', .group=4, .name="roman", .flags=OPTION_ALIAS},

    // Space
    {.key=' ', .group=5, .name=" ", .flags=OPTION_DOC | OPTION_NO_USAGE},

    // Bounds
    {
        .key='l', .group=6, .name="lo", .arg="value",
        .doc=N_("Lower bound for the guessing game"),
    },
    {
        .key='h', .group=7, .name="hi", .arg="value",
        .doc=N_("Upper bound for the guessing game"),
    },

    // Space
    {.key=' ', .group=8, .name=" ", .flags=OPTION_DOC | OPTION_NO_USAGE},

    // Help, Usage, Version
    {.key='\0', NULL},
};

const char *argp_program_version = PACKAGE_STRING; ///< Version string.
const char *argp_program_bug_address = "<" PACKAGE_BUGREPORT ">"; ///< E-mail.

/** Argp parser specification. */
const struct argp argp =
{
    .doc=N_(
        "\n"
        "Play a simple number guessing game with the computer. You will be "
        "asked to pick a random number and then answer a series of questions "
        "about that number, after which the program will guess the number you "
        "picked.\n"
        "\n"
        "You can configure, which numbering system will be used by the program "
        "and the lower and upper bounds for your number using the following "
        "options:"
        "\v"
        "Examples:\n"
        "\n"
        "  guess --lo=42 --hi=69\n"
        "\n"
        "  guess --mode=x --lo=0xBEEF --hi=0xDEAD\n"
        "\n"
        "  guess --roman --lo=N --hi=M\n"
    ),
    .parser=parser,
    .options=options,
};

/** @note Defaults to "decimal numbers from 1 to 100". */
settings_t settings =
{
    .mode = MODE_DEC,
    .lo = 1,
    .hi = 100,
};

void parse_args(int argc, char *argv[])
{
    if (argp_parse(&argp, argc, argv, 0, NULL, NULL))
        exit(1);
}

// The following section defines xgettext-able strings that are not used
// explicitly anywhere in this program, but whose translations are used
// indirectly by gettext.

S_("Usage:")
S_(" [OPTION...]")

S_("Give this help list")
S_("Give a short usage message")
S_("Print program version")

S_(
    "Mandatory or optional arguments to long options are also mandatory or "
    "optional for any corresponding short options."
)

S_("Report bugs to %s.\n")
