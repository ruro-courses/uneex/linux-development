#include "common.h"

#include "cmdline.h"
#include "interact.h"
#include "numbers.h"

/** @addtogroup guess */
/** @{ */
/** @file guess.c */

/** Actual guessing game logic for a single game. */
void play(void)
{
    char *s_lo = num_to_str(settings.lo);
    char *s_hi = num_to_str(settings.hi);
    printf(
        _(
            "Pick any number between %s and %s (inclusive).\n"
            "I will ask you some questions and try to guess your number.\n"
            "\n"
        ),
        s_lo, s_hi
    );
    free(s_lo);
    free(s_hi);

    int lo = settings.lo, hi = settings.hi;
    while (lo < hi)
    {
        int middle = (lo + hi) / 2;
        char *s_middle = num_to_str(middle);

        if (ask_yn(_("Is your number larger than %s?"), s_middle))
            lo = 1 + middle;
        else
            hi = middle;

        free(s_middle);
    }

    char *s_num = num_to_str(lo);
    printf(_("Your number is %s!\n\n"), s_num);
    free(s_num);
}

/** Entrypoint. */
int main(int argc, char *argv[])
{
    const char *localedir = getenv("LOCALEDIR");
    if (!localedir)
        localedir = LOCALEDIR;

    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE, localedir);
    textdomain(PACKAGE);

    parse_args(argc, argv);
    while (true)
    {
        play();

        if (!ask_yn(_("Do you want to play again?")))
            goodbye();
    }
}

/** @} */
