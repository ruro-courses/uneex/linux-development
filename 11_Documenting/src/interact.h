#pragma once
#include "common.h"

/** @addtogroup interact */
/** @{ */
/** @file interact.h */

/** Print a goodbye message and exit. */
__attribute__((noreturn)) void goodbye(void);

/** Ask a localized yes/no question from the user.
 *
 * Uses @ref smprintf to format the question and readline for user interaction.
 * Will keep asking until the user gives a valid answer or disconnects.
 */
bool ask_yn(const char *format, ...);

/** @} */
