#pragma once
#include "common.h"

/** @addtogroup numbers */
/** @{ */
/** @file numbers.h */

/** Enumeration of all possible representations for number as strings. */
typedef enum
{
    MODE_DEC, ///< Decimal (9, 10, 11)
    MODE_OCT, ///< Octal (07, 010, 011)
    MODE_HEX, ///< Hexadecimal (0xf, 0x10, 0x11)
    MODE_ROM, ///< Roman numeral (VIII, IX, X) @see roman.h
} number_t;

/** Parse the number in string str and return its numerical value.
 *
 * If str is not a valid number, return -1 instead.
 */
int str_to_num(const char *str);

/** Convert the number num to a representation string.
 *
 * The type of the representation string is determined by @ref settings.
 *
 * @warning The returned string is allocated on the heap with malloc.
 * It is the responsibility of the user to free this memory after use.
 */
char *num_to_str(int num);

/** @} */
