#include "interact.h"
#include "smprintf.h"

/** @addtogroup interact */
/** @{ */
/** @file interact.c */

void goodbye(void)
{
    printf(_("Thank you for playing. Goodbye!\n"));
    exit(0);
}

bool ask_yn(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    char *question = vsmprintf(format, args);
    va_end(args);

    const char *y = _("yes");
    const char *n = _("no");
    char *prompt = smprintf("%s [%s/%s] ", question, y, n);
    free(question);

    bool result;
    while (true)
    {
        char *answer = readline(prompt);
        if (answer == NULL)
        {
            free(prompt);
            goodbye();
        }
        else if (!strcasecmp(answer, y))
            result = true;
        else if (!strcasecmp(answer, n))
            result = false;
        else
        {
            printf(
                _("'%s' is not a valid answer. Please, try again.\n"),
                answer
            );
            free(answer);
            continue;
        }

        add_history(answer);
        free(answer);
        break;
    }

    free(prompt);
    return result;
}

/** @} */
