#include "numbers.h"

#include "cmdline.h"
#include "roman.h"
#include "smprintf.h"

/** @addtogroup numbers */
/** @{ */
/** @file numbers.c */

char *num_to_str(int num)
{
    switch (settings.mode)
    {
    case MODE_DEC:
        return smprintf("%d", num);
    case MODE_OCT:
        return smprintf("0%o", num);
    case MODE_HEX:
        return smprintf("0x%X", num);
    case MODE_ROM:
        return decimal_to_roman(num);
    default:
        __builtin_unreachable();
    }
}

int str_to_num(const char *str)
{
    if (!*str)
        return -1;

    char *end = NULL;
    long num = strtol(str, &end, 0);
    if (*str && !*end && INT_MIN + 1 < num && num < INT_MAX - 1)
        return (int) num;

    return roman_to_decimal(str);
}

/** @} */
