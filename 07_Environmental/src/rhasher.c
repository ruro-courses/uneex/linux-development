
#include "common.h"
#include "input_output.h"
#include "supported_hashes.h"

/** region Typedefs & Forward declarations */
typedef bool callback_t(int argc, char *argv[]);
typedef struct {const char *name; callback_t *impl; const char *desc;} dispatch_t;

bool hash(unsigned rhash_id, int argc, char *argv[]);
dispatch_t dispatch[];

callback_t help;
callback_t quit;
callback_t do_dispatch;

#define f(hi, lo) \
callback_t lo ## _impl;
FOREACH_HASH(f)
#undef f

void main_usage(void);
void hash_usage(char *prog);
/** endregion */

/** region Compute hash */
bool hash(unsigned rhash_id, int argc, char *argv[])
{
    if (argc != 3)
    { hash_usage(argv[0]); return true; }

    int flags = isupper(argv[0][0]) ? RHPR_HEX : RHPR_BASE64;
    bool is_file = !strcasecmp(argv[1], "--file");
    bool is_string = !strcasecmp(argv[1], "--string");

    if (!(is_string || is_file))
    { hash_usage(argv[0]); return true; }

    uint8_t digest[64];
    if (is_string && rhash_msg(rhash_id, argv[2], strlen(argv[2]), digest) < 0)
    {
        wrn(argv[0], "Message digest calculation error.");
        return true;
    }
    if (is_file && rhash_file(rhash_id, argv[2], digest) < 0)
    {
        wrn(argv[0], "Couldn't hash '%s':\n%s", argv[2], strerror(errno));
        return true;
    }

    if (is_string || is_file)
    {
        char output[130];
        size_t digest_size = rhash_get_digest_size(rhash_id);
        rhash_print_bytes(output, digest, digest_size, flags);
        printf("%s\n", output);
    }
    else
        hash_usage(argv[0]);
    return true;
}
/** endregion */

/** region Commands */
dispatch_t dispatch[] =
{
    // Generic commands
    {.name="help", .impl=help, .desc="Print this message."},
    {.name="exit", .impl=quit, .desc="Exit the program."},
    {.name="quit", .impl=quit, .desc="Exit the program."},

    // Hash implementations
    #define f(hi, lo) \
    {.name=#lo, .impl=lo ## _impl, .desc="Compute the " #hi " hash."},
    FOREACH_HASH(f)
    #undef f
};

bool help(int argc, char *argv[]) { (void) argc; (void) argv; main_usage(); return true; }
bool quit(int argc, char *argv[]) { (void) argc; (void) argv; return false; }

#define f(hi, lo) \
bool lo ## _impl (int argc, char *argv[]) \
{ return hash( RHASH_ ## hi , argc, argv); return true; }
FOREACH_HASH(f)
#undef f
/** endregion */

/** region Help and Usage */
void main_usage(void)
{
    hash_usage("<hash>");

    size_t dispatch_size = sizeof(dispatch)/sizeof(dispatch[0]);
    wrn("Available hashes", "");
    for (size_t i = 3; i < dispatch_size; ++i)
    {
        if (i % 10 == 3)
            fprintf(stderr, "\n    ");
        fprintf(stderr, "%s ", dispatch[i].name);
    }

    fprintf(stderr, "\n\n");
    wrn("Other commands", "");
    for (size_t i = 0; i < 3; ++i)
        fprintf(stderr, "    %16s - %s\n", dispatch[i].name, dispatch[i].desc);
}

void hash_usage(char *prog)
{
    wrn(
        "Usage",
        "\n"
        "    %s --file [filename]\n"
        "    %s --string [string]\n"
        "\n"
        "    Depending on the case of the first letter of the %s command,\n"
        "    print either the Base64 or Hexadecimal representation of the hash.\n"
        "\n"
        "Examples:\n"
        "    %s --file README.md\n"
        "    %s --string foo\n"
        "    %s --string 'This is a string!'\n"
        "    %s --string \"This works: ${PATH}\"\n"
        "    %s --string \"And so does this: $(head -c 16 /dev/urandom)\"\n",
        prog, prog, prog, prog, prog, prog, prog, prog
    );
}
/** endregion */

/** region REPL **/
bool do_dispatch(int argc, char *argv[])
{
    if (!argc) return true;

    size_t dispatch_size = sizeof(dispatch)/sizeof(dispatch[0]);
    for (size_t i = 0; i < dispatch_size; ++i)
    {
        dispatch_t d = dispatch[i];
        if (!strcasecmp(argv[0], d.name))
            return d.impl(argc, argv);
    }

    wrn(PACKAGE_TARNAME, "Command not found: %s", argv[0]);
    main_usage();
    return true;
}

int main(int argc, char *argv[])
{
    rhash_library_init();

    if (argc != 1)
        return !do_dispatch(argc - 1, argv + 1);

    char *msg = NULL;
    wordexp_t result = {.we_wordc = 0, .we_wordv = NULL, .we_offs = 0};
    while (true)
    {
        free(msg);
        wordfree(&result);

        msg = readline("> ");
        if (*msg)
            add_history(msg);

        switch (wordexp(msg, &result, WRDE_UNDEF))
        {
        case 0:
            break;
        case WRDE_BADCHAR:
            wrn(PACKAGE_TARNAME, "Unexpected token.");
            continue;
        case WRDE_BADVAL:
            wrn(PACKAGE_TARNAME, "Unbound variable.");
            continue;
        case WRDE_SYNTAX:
            wrn(PACKAGE_TARNAME, "Syntax error.");
            continue;
        default:
            err("Invalid wordexp return value.");
        }

        if (!do_dispatch(result.we_wordc, result.we_wordv))
            break;
    }
}
/** endregion */
