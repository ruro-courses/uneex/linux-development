#include "input_output.h"

#if !defined(HAVE_LIBREADLINE)
char *readline(const char *prompt)
{
    printf("%s", prompt);

    char *line = NULL;
    size_t n = 0;
    if (getline(&line, &n, stdin) < 0)
        err("Couldn't read from stdin");

    size_t len = strlen(line);
    if (line[len - 1] == '\n')
        line[len - 1] = '\0';

    return line;
}
#endif
