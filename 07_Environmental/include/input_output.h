#pragma once

#include "common.h"

#define wrn(name, format, ...)                                                  \
do {                                                                            \
    fprintf(stderr, "%s: " format "\n", name, ## __VA_ARGS__);                  \
} while (false)

#define err(format, ...)                                                        \
do {                                                                            \
    error_at_line(                                                              \
        1, errno,                                                               \
        __FILE__, __LINE__ - 1,                                                 \
        "\b%s:\n    "  format "\n",                                             \
        __func__, ## __VA_ARGS__                                                \
    );                                                                          \
} while (false)

#if defined(DISABLE_REDLINE)
#undef HAVE_LIBREADLINE
#endif

#if !defined(HAVE_LIBREADLINE)
char *readline(const char *prompt);
#define add_history(line) do {} while (false)
#else
#include <readline/readline.h>
#include <readline/history.h>
#endif
