#pragma once

#include "common.h"

#define FOREACH_HASH(func)                                                      \
    func(AICH,             aich)                                                \
    func(BLAKE2B,          blake2b)                                             \
    func(BLAKE2S,          blake2s)                                             \
    func(BTIH,             btih)                                                \
    func(CRC32,            crc32)                                               \
    func(CRC32C,           crc32c)                                              \
    func(ED2K,             ed2k)                                                \
    func(EDONR256,         edonr256)                                            \
    func(EDONR512,         edonr512)                                            \
    func(GOST12_256,       gost12_256)                                          \
    func(GOST12_512,       gost12_512)                                          \
    func(GOST94,           gost94)                                              \
    func(GOST94_CRYPTOPRO, gost94_cryptopro)                                    \
    func(HAS160,           has160)                                              \
    func(MD4,              md4)                                                 \
    func(MD5,              md5)                                                 \
    func(RIPEMD160,        ripemd160)                                           \
    func(SHA1,             sha1)                                                \
    func(SHA224,           sha224)                                              \
    func(SHA256,           sha256)                                              \
    func(SHA384,           sha384)                                              \
    func(SHA3_224,         sha3_224)                                            \
    func(SHA3_256,         sha3_256)                                            \
    func(SHA3_384,         sha3_384)                                            \
    func(SHA3_512,         sha3_512)                                            \
    func(SHA512,           sha512)                                              \
    func(SNEFRU128,        snefru128)                                           \
    func(SNEFRU256,        snefru256)                                           \
    func(TIGER,            tiger)                                               \
    func(TTH,              tth)                                                 \
    func(WHIRLPOOL,        whirlpool)
