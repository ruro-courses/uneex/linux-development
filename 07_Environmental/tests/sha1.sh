#!/bin/sh

set -ex

cmp \
    <(./rhasher SHA1 --string "foo") \
    <(echo -ne "foo" | sha1sum - | tr -d ' -')

cmp \
    <(./rhasher SHA1 --file README.md) \
    <(cat README.md | sha1sum - | tr -d ' -')
