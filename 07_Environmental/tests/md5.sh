#!/bin/sh

set -ex

cmp \
    <(./rhasher MD5 --string "foo") \
    <(echo -ne "foo" | md5sum - | tr -d ' -')

cmp \
    <(./rhasher MD5 --file README.md) \
    <(cat README.md | md5sum - | tr -d ' -')
