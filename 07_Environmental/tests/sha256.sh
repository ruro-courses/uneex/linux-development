#!/bin/sh

set -ex

cmp \
    <(./rhasher SHA256 --string "foo") \
    <(echo -ne "foo" | sha256sum - | tr -d ' -')

cmp \
    <(./rhasher SHA256 --file README.md) \
    <(cat README.md | sha256sum - | tr -d ' -')
