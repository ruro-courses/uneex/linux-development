#!/bin/sh

set -ex

cmp \
    <(./rhasher SHA512 --string "foo") \
    <(echo -ne "foo" | sha512sum - | tr -d ' -')

cmp \
    <(./rhasher SHA512 --file README.md) \
    <(cat README.md | sha512sum - | tr -d ' -')
