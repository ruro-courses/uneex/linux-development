#!/bin/sh

# Check usage
if [ "${#}" -gt 1 ] || [ -t 0 ]; then
    printf "Usage:\n\tcat [input] | ${0} [delay]\n" >&2; exit 1
fi
DELAY="${1:-0}"

# Read input char-by-char
printf "Reading the input file, please wait.\n"
TEXT=$(cat - | hexdump -ve '/1 "%o\n"')

# Index each character by line number and character number
LINE_NO=0
CHAR_NO=0
INDEXED_TEXT=""
while read CHAR; do
    if [ "${CHAR}" -ne 40 ]; then
        INDEXED_TEXT=$(printf "%s %s %s\n%s" "${LINE_NO}" "${CHAR_NO}" "${CHAR}" "${INDEXED_TEXT}")
    fi
    CHAR_NO=$((CHAR_NO + 1))
    if [ "${CHAR}" -eq 12 ]; then
        LINE_NO=$((LINE_NO + 1)); CHAR_NO=0
    fi
done <<EOF
${TEXT}
EOF

# Put the single characters on the screen (in random order)
put() {
    tput cup "${1}" "${2}"
    printf '\'"${3}"
}

tput clear
shuf <<EOF |
${INDEXED_TEXT}
EOF
    while read CHAR2; do
        put ${CHAR2}; sleep ${DELAY}
    done

# Move the cursor to the end
tput cup "${LINE_NO}" "0"
