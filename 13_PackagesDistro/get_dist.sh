#!/bin/sh
cd "$(dirname "${0}")"
set -eu

git clean -xdf

ROOT=../11_Documenting/
NAME=$(grep -o "^AC_INIT[^,]*" "${ROOT}/configure.ac" | grep -o "\[.*\]" | tr -d '][')
VERSION=$(grep "^AC_INIT" "${ROOT}/configure.ac" | grep -oE "[0-9]+\.[0-9]+\.[0-9]+")
IDENT="${NAME}-${VERSION}"

cp -r ../11_Documenting/ ./"${IDENT}"
tar --mtime=@0 -czf ./"${IDENT}.tar.gz" ./"${IDENT}"

rm -rf ./"${IDENT}"
