Сторонние исходники и установка
===============================

Текст задания
-------------

1. Взять решение Д/З с локализацией и обеспечить в нём
    1. Примитивный man (любым способом)
    2. Сборку и установку в произвольный каталог (для autotools — с помощью --prefix), содержащий стандартные подкаталоги
        - Убедиться, что установленное таком образом приложение находит файлы с переводами в подкаталогах этого каталога
    3. Сборку по стандартной схеме (для autotools это делается с помощью --prefix=/usr) и установку в произвольный подкаталог (для autotools это делается с помощью make DESTDIR=произвольный_каталог)
        - Убедиться, что
            1. бинарники установлены в произвольный_каталог/usr/bin/,
            2. русские переводы установлены в произвольный_каталог/usr/share/locale/ru/LC_MESSAGES/
            3. manpage установлена в произвольный_каталог/usr/share/man/man1/
        - Убеждаться в этом не обязательно, но собранная таким образом программа должна искать перевод в /usr/share/locale/ru/LC_MESSAGES/…
    4. Полученный код, очищенный от генератов, поместить в подкаталог 12_PatchInstall отчётного git-репозитория

Результат
---------

Данный функционал уже был реализован в [прошлом задании](../11_Documenting/).

Сборка:
```
cd 11_Documenting
autoreconf -si
./configure --prefix=/arbitrary/prefix
make
```

Установка:
```
make DESTDIR=$(realpath -m relative/destdir) install
```

Проверка наличия нужных файлов:
```
> find ./relative -type f
./relative/destdir/arbitrary/prefix/share/man/man1/guess.1
./relative/destdir/arbitrary/prefix/share/locale/ru/LC_MESSAGES/guess.mo
./relative/destdir/arbitrary/prefix/bin/guess
```

Запуск (`LOCALEDIR` тут нужен просто потому что `DESTDIR` не корень системы):
```
> LOCALEDIR="./relative/destdir/arbitrary/prefix/share/locale" LANGUAGE="ru_RU" ./relative/destdir/arbitrary/prefix/bin/guess --help
Использование: guess [ПАРАМЕТР...]

<SNIP>
```
