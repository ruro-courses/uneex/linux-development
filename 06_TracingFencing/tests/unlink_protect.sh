#!/bin/sh

TEMP=$(mktemp -d)

base64 /dev/urandom | head > "${TEMP}/source_PROTECT_postfix"
cp "${TEMP}/source_PROTECT_postfix" "${TEMP}/source.bak"

LD_PRELOAD=./libunlink_protect.so ./move \
    "${TEMP}/source_PROTECT_postfix" \
    "${TEMP}/destination" \
        && \
        cmp \
            "${TEMP}/source.bak" \
            "${TEMP}/destination" \
        && \
        cmp \
            "${TEMP}/source.bak" \
            "${TEMP}/source_PROTECT_postfix" \
