#!/bin/sh

TEMP=$(tests/_common.sh destination close EIO)
cmp "${TEMP}/source" "${TEMP}/source.bak" \
    && cmp "${TEMP}/source" "${TEMP}/destination"
