#!/bin/sh

TEMP=$(tests/_common.sh source openat EPERM)
cmp "${TEMP}/source" "${TEMP}/source.bak" \
    && test ! -e "${TEMP}/destination"
