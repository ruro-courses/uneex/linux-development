#!/bin/sh

TEMP=$(mktemp -d)

base64 /dev/urandom | head > "${TEMP}/source"
cp "${TEMP}/source" "${TEMP}/source.bak"

./move \
    "${TEMP}/source" \
    "${TEMP}/destination" \
        && \
        cmp \
            "${TEMP}/source.bak" \
            "${TEMP}/destination" \
        && \
        test ! -e "${TEMP}/source"
