#!/bin/sh

TEMP=$(tests/_common.sh source unlink EIO)
cmp "${TEMP}/source" "${TEMP}/source.bak" \
    && cmp "${TEMP}/source" "${TEMP}/destination"
