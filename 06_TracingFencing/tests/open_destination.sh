#!/bin/sh

TEMP=$(tests/_common.sh destination openat EPERM)
cmp "${TEMP}/source" "${TEMP}/source.bak" \
    && test ! -e "${TEMP}/destination"
