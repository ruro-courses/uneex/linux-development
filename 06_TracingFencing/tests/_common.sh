#!/bin/sh

TEMP=$(mktemp -d)

base64 /dev/urandom | head > "${TEMP}/source"
cp "${TEMP}/source" "${TEMP}/source.bak"

strace \
    -P "${TEMP}/${1}" \
    -e fault=${2}:error=${3} \
    ./move \
    "${TEMP}/source" \
    "${TEMP}/destination" \
    >/dev/null 2>/dev/null \
        || true

echo "${TEMP}"
