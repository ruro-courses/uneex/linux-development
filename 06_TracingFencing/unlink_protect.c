#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>

typedef int unlink_proto(const char *);
unlink_proto unlink;

int unlink(const char *pathname)
{
    if (strstr(pathname, "PROTECT")) return 0;

    unlink_proto *original_unlink = dlsym(RTLD_NEXT, "unlink");
    return original_unlink(pathname);
}
