#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

void or_die(const char *reason)
{
    int status = 0;
    switch (errno)
    {
    case 0:
        break;

    case EINVAL:
    // fall through
    case EISDIR:
    // fall through
    case ELOOP:
    // fall through
    case ENAMETOOLONG:
        status = EX_USAGE;
        break;

    case EACCES:
        status = EX_NOPERM;
        break;

    case ENOENT:
        status = EX_NOINPUT;
        break;

    default:
        status = EX_SOFTWARE;
        break;
    }

    if (status)
    {
        perror(reason);
        exit(status);
    }
}

const size_t BUF_SIZE = 8 * 1024; // 8 KiB

void move(const char *src, const char *dst)
{
    int f_src = open(src, O_RDONLY);
    or_die("Couldn't open the source file");

    int f_dst = open(dst, O_CREAT | O_WRONLY | O_TRUNC, 0666);
    or_die("Couldn't open the destination file");

    ssize_t num_read = 0;
    char buf[BUF_SIZE];
    while ((num_read = read(f_src, buf, BUF_SIZE)))
    {
        or_die("Couldn't read data from source file");

        write(f_dst, buf, num_read);
        or_die("Couldn't write data to destination file");
    }

    close(f_dst);
    or_die("Couldn't close destination file");

    close(f_src);
    or_die("Couldn't close source file");

    unlink(src);
    or_die("Couldn't unlink source file");
}

int main(int argc, const char *argv[])
{
    if (argc != 3)
    {
        fprintf(
            stderr,
            "Usage:\n"
            "\t%s [source] [destination]\n",
            argv[0]
        );
        return EX_USAGE;
    }
    move(argv[1], argv[2]);
}
