#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

typedef struct
{
    int _i;
    int start;
    int stop;
    int step;
} range;

void usage(const char *progname)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s STOP\n", progname);
    fprintf(stderr, "\t%s START STOP\n", progname);
    fprintf(stderr, "\t%s START STOP STEP\n", progname);
    exit(EX_USAGE);
}

int strtoi_or_die(const char *str)
{
    char *str_end = 0;
    errno = 0;
    long result = strtol(str, &str_end, 10);
    if (errno == ERANGE || result > INT_MAX || result < INT_MIN)
    {
        fprintf(stderr, "Integer argument outside valid range.\n");
        exit(EX_USAGE);
    }
    if (errno || *str_end)
    {
        fprintf(stderr, "Expected integer, got '%s'.\n", str);
        exit(EX_USAGE);
    }
    return result;
}

void argparse(int argc, const char *argv[], int *start, int *stop, int *step)
{
    if (argc < 2 || argc > 4) usage(argv[0]);

    *start = 0;
    *stop = strtoi_or_die(argv[1]);
    *step = 1;

    if (argc > 2) { *start = *stop; *stop = strtoi_or_die(argv[2]); }
    if (argc == 4) *step = strtoi_or_die(argv[3]);
    if (!*step)
    {
        fprintf(stderr, "STEP must not be zero.\n");
        exit(EX_USAGE);
    }
}

void range_init(range *I) { I->_i = I->start; }
bool range_run(range *I) { return I->step > 0 ? I->_i < I->stop : I->_i > I->stop; }
void range_next(range *I) { I->_i += I->step; }
int range_get(range *I) { return I->_i; }

int main(int argc, const char *argv[])
{
    range I;
    argparse(argc, argv, &I.start, &I.stop, &I.step);
    for(range_init(&I); range_run(&I); range_next(&I))
        printf("%d\n", range_get(&I));
    return 0;
}
