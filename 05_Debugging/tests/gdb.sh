#!/bin/bash

cmp \
    <(gdb -batch -x tests/${1}.gdb) \
    tests/${1}.out
