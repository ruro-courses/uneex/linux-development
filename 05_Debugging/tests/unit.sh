#!/bin/bash

if [ "${#}" -eq 0 ];
then
    # Don't test the 0-argument case
    exit 0
fi

cmp \
    <(./range "${@}") \
    <(python -c "
import sys
for i in range(*(int(e) for e in sys.argv[1:])):
    print(i)
" "${@}")
