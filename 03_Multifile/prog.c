#include <stdio.h>
#include "outlib.h"


int main(int argc, const char *argv[])
{
    Count = argc;
    if(argc > 1)
    {
        output("<INIT>");
        for(int i = 1; i < argc; ++i) output(argv[i]);
        output("<DONE>");
    }
    else usage(argv[0]);
    return 0;
}
