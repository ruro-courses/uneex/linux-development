#include <locale.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

/** Here's a really really long line that is placed here for testing purposes. Normally it would be cut off by the right side of your screen, but you can use the arrow keys to scroll to the right and to the left and see the rest of the line. Cool, huh? */

/** region Typedefs */
typedef void callback_t(void);
typedef struct {int key; callback_t *callback;} dispatch_t;
/** endregion */

/** region Callback forward declarations */
callback_t invalid_key;
callback_t quit;

callback_t mv_xneg;
callback_t mv_xpos;
callback_t mv_yneg;
callback_t mv_ypos;

callback_t mv_xneg_page;
callback_t mv_xpos_page;

callback_t resize;
/** endregion */

/** region Global state */
WINDOW *root = 0;
WINDOW *bbox = 0;
WINDOW *text = 0;
bool done = false;
const char *filename = 0;
char *contents = 0;

int bbox_scr_h = 0;
int bbox_scr_w = 0;
int bbox_scr_x = 0;
int bbox_scr_y = 0;

/// Content pad (screen size)
int text_scr_h = 0;
int text_scr_w = 0;
int text_scr_x = 0;
int text_scr_y = 0;

/// Content pad (full text size)
int text_dat_h = 0;
int text_dat_w = 0;
int text_dat_x = 0;
int text_dat_y = 0;
/** endregion */

/** region Regular functions */
void readlines(const char *filename)
{
    /// Read file
    FILE *file = fopen(filename, "rb");
    if (!file) { perror("Error opening file"); exit(EX_OSFILE); }

    fseek(file, 0, SEEK_END);
    int fsize = ftell(file);
    fseek(file, 0, SEEK_SET);
    contents = malloc(1 + fsize);
    fread(contents, 1, fsize, file);
    contents[fsize] = 0;
    fclose(file);

    /// Count lines and find the longest line
    text_dat_h = 0;
    text_dat_w = 0;
    int current_y = 0;
    int offset = 0;
    while (offset < fsize)
    {
        if ((1 + offset) == fsize || contents[offset] == '\n')
        {
            text_dat_h += 1;
            text_dat_w = text_dat_w > current_y ? text_dat_w : current_y;
            current_y = 0;
        }
        else
        {
            current_y += 1;
        }
        offset += 1;
    }
}

int clip(int x, int lo, int hi)
{
    if (x >= hi) x = hi;
    if (x <= lo) x = lo;
    return x;
}

void update(int dx, int dy)
{
    text_dat_x = clip(text_dat_x + dx, 0, text_dat_h - text_scr_h);
    text_dat_y = clip(text_dat_y + dy, 0, text_dat_w - text_scr_w);

    wmove(root, 0, 0);
    wclrtoeol(root);
    wprintw(root, "file: %s [x: %d, y: %d]", filename, text_dat_x, text_dat_y);

    mvwin(bbox, bbox_scr_x, bbox_scr_y);
    wresize(bbox, bbox_scr_h, bbox_scr_w);
    box(bbox, 0, 0);

    prefresh(
        text,
        text_dat_x, text_dat_y,
        text_scr_x, text_scr_y,
        1+text_scr_h, text_scr_w
    );
}

void wprintw_but_not_stupid(WINDOW *win_, const char *s)
{
    /// This function is a workaround for a stupid bug in ncurses that doesn't
    /// allow you to print more than LINES * (COLS+1) characters at once with
    /// wprintw due to an undersized internal buffer.

    int bufsize = LINES * (COLS+1) - 1;
    int remaining_len = strlen(s);
    while (remaining_len > bufsize)
    {
        wprintw(win_, "%.*s", bufsize, s);
        s += bufsize;
        remaining_len -= bufsize;
    }
    wprintw(win_, "%s\n", s);
}

void recalc_sizes(void)
{
    int h = LINES;
    int w = COLS;
    int x = 0;
    int y = 0;

    /// A single line is reserved for the header text in the root window
    h -= 1;
    x += 1;
    bbox_scr_h = h;
    bbox_scr_w = w;
    bbox_scr_x = x;
    bbox_scr_y = y;

    /// A single character on each side is reserved for the bounding box outline
    h -= 2;
    w -= 2;
    x += 1;
    y += 1;
    text_scr_x = x;
    text_scr_y = y;
    text_scr_h = h;
    text_scr_w = w;
}

int main(int argc, const char *argv[])
{
    /** region Parse arguments */
    if (argc != 2)
    {
        printf(
            "Usage:\n"
            "\t%s [filename]\n",
            argv[0]
        );
        return EX_USAGE;
    }
    filename = argv[1];
    readlines(filename);
    /** endregion */

    /** region Root window */
    setlocale(LC_ALL, "en_US.UTF-8");
    root = initscr();
    noecho();
    cbreak();
    set_escdelay(0);
    curs_set(0);
    keypad(root, true);
    recalc_sizes();
    /** endregion */

    /** region Bounding box */
    bbox = subwin(root, bbox_scr_h, bbox_scr_w, bbox_scr_x, bbox_scr_y);
    box(bbox, 0, 0);
    /** endregion */

    /** region Text content */
    text = newpad(text_dat_h, text_dat_w + 1);
    wprintw_but_not_stupid(text, contents);
    free(contents);
    resize();
    /** endregion */

    /** region Key bindings */
    callback_t *default_callback = invalid_key;
    dispatch_t dispatch[] =
    {
        /// Q/ESC - quit
        {.key='q', .callback=quit},
        {.key='\e', .callback=quit},

        /// Space/Enter/Down - next line
        {.key=' ', .callback=mv_xpos},
        {.key='\n', .callback=mv_xpos},
        {.key=KEY_ENTER, .callback=mv_xpos},
        {.key=KEY_DOWN, .callback=mv_xpos},

        /// Other directions
        {.key=KEY_UP, .callback=mv_xneg},
        {.key=KEY_RIGHT, .callback=mv_ypos},
        {.key=KEY_LEFT, .callback=mv_yneg},

        /// Scroll Wheel Up/Scroll Wheel Down
        {.key=BUTTON4_PRESSED, .callback=mv_xneg},
        {.key=BUTTON5_PRESSED, .callback=mv_xpos},

        /// Page Up/Page Down
        {.key=KEY_PPAGE, .callback=mv_xneg_page},
        {.key=KEY_NPAGE, .callback=mv_xpos_page},

        /// Resize event
        {.key=KEY_RESIZE, .callback=resize},
    };
    size_t dispatch_size = sizeof(dispatch)/sizeof(dispatch[0]);
    /** endregion */

    /** region Event loop */
    while (!done)
    {
        int ch = wgetch(root);
        callback_t *callback = default_callback;

        for (size_t i = 0; i < dispatch_size; ++i)
        {
            dispatch_t d = dispatch[i];
            if (d.key == ch) { callback = d.callback; break; }
        }

        callback();
    }
    /** endregion */

    endwin();
}
/** endregion */

/** region Callback implementations */
void invalid_key(void) { beep(); flash(); }
void quit(void) { done = true; }

void mv_xneg(void) { update(-1, +0); }
void mv_xpos(void) { update(+1, +0); }
void mv_yneg(void) { update(+0, -1); }
void mv_ypos(void) { update(+0, +1); }

void mv_xneg_page(void) { update(-text_scr_h, +0); }
void mv_xpos_page(void) { update(+text_scr_h, +0); }

void resize(void)
{
    recalc_sizes();
    update(0,0);
    wrefresh(bbox);
    wrefresh(root);
    update(0,0);
}
/** endregion */
