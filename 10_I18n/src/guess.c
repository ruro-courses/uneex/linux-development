#include <locale.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include <libintl.h>
#include <readline/readline.h>

#include "config.h"

#define STR_(x) #x
#define STR(x) STR_(x)
#define LO 1
#define HI 100
#define PAD (int)(sizeof(STR(HI)) - 1)
#define _(x) gettext(x)

char *vsmprintf(const char *format, va_list args1)
{
    va_list args2;
    va_copy(args2, args1);

    size_t size = 1 + vsnprintf(NULL, 0, format, args1);

    char *buffer = malloc(size);
    vsnprintf(buffer, size, format, args2);

    return buffer;
}

char *smprintf(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    char *buffer = vsmprintf(format, args);
    va_end(args);
    return buffer;
}

void goodbye(void)
{
    printf(_("Thank you for playing. Goodbye!\n"));
    exit(0);
}

bool ask_yn(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    char *question = vsmprintf(format, args);
    va_end(args);

    const char *y = _("yes");
    const char *n = _("no");
    char *prompt = smprintf("%s [%s/%s] ", question, y, n);
    free(question);

    bool result;
    while (true)
    {
        char *answer = readline(prompt);
        if (answer == NULL)
        {
            free(prompt);
            goodbye();
        }
        else if (!strcasecmp(answer, y)) result = true;
        else if (!strcasecmp(answer, n)) result = false;
        else
        {
            printf(
                _("'%s' is not a valid answer. Please, try again.\n"),
                answer
            );
            free(answer);
            continue;
        }
        free(answer);
        break;
    }

    free(prompt);
    return result;
}

void play(void)
{
    printf(
        _(
            "Pick any number between %*d and %*d (inclusive).\n"
            "I will ask you some questions and try to guess your number.\n"
            "\n"
        ),
        PAD, LO, PAD, HI
    );
    int lo = LO, hi = HI;
    while (lo < hi)
    {
        int middle = (lo + hi) / 2;
        if (ask_yn(_("Is your number larger than %*d?"), PAD, middle))
            lo = 1 + middle;
        else
            hi = middle;
    }
    printf(_("Your number is %d!\n\n"), lo);
}

int main(void)
{
    const char *localedir = getenv("LOCALEDIR");
    if (!localedir)
        localedir = LOCALEDIR;

    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE, localedir);
    textdomain(PACKAGE);

    while (true)
    {
        play();

        if (!ask_yn(_("Do you want to play again?"))) goodbye();
    }
}
